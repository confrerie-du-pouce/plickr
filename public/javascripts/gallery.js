"use strict";

window.onload = function() {
    document.getElementById('checkJS').remove();
    document.getElementById('up').multiple = true;
    document.getElementById('fileupload').addEventListener('submit', function (ev) {
        ev.preventDefault();
        let progress = document.getElementById("progress");
        let form = document.getElementById('fileupload');
        let data = new FormData(form);
        let image = document.getElementById("up");
        let pics = "";
        for(let i = 0 ; i < image.files.length ; i++){
            let im = image.files[i];
            data.append('image' + i,im,im.name);
            if (pics !== "") pics += ',';
            pics += im.name;
        }
        let xhr = new XMLHttpRequest();
        xhr.open("POST", form.getAttribute('action'), true);
        xhr.upload.onprogress = function(e){
            if(e.lengthComputable){
                progress.value = (e.loaded / e.total) * 100;
            }
        };
        xhr.onload = function () {
            if (xhr.status === 200) {
                // File(s) uploaded.
                document.location.href="./?p=upload&a=check&pics=" + pics;
                replaceAll(xhr.response);
            } else {
                alert('An error occurred!');
            }
        };
        xhr.send(data);
    }, false);

    document.getElementById("up").addEventListener('change' , function f(ev) {
        let file = this.files;
        let output = document.getElementById('uploadedimage');
        if (!file.length) {
            output.innerHTML = "<p>pas d'image séléctionnée!</p>";
        } else {
            output.innerHTML = "";
            let list = document.createElement("ul");
            for(let i = 0 ; i < file.length; i++){
                let item = document.createElement("li");
                let img = document.createElement("img");
                img.src = window.URL.createObjectURL(file[i]);
                img.height = 150;
                img.onload = function () {
                    window.URL.revokeObjectURL(this.src);
                };
                item.appendChild(img);
                list.appendChild(item);
            }
            output.append(list);
        }
    });
};

function replaceAll(content) {
    document.open();
    document.write(content);
    document.close();
}
