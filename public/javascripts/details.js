"use strict";

// Initialize and add the map
function initMap() {
    let div = document.getElementById('map');
    let latitude = parseFloat(div.getAttribute('data-lat'));
    let longitude = -parseFloat(div.getAttribute('data-lon'));
    if (latitude !== 0.0 && longitude !== 0.0) {
        let coord = {lat: latitude, lng: longitude};
        let map = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: coord});
        let marker = new google.maps.Marker({position: coord, map: map});
    } else {
        div.style.height = '1.3em';
        div.style.textAlign = 'center';
        div.innerHTML = "<h3>Aucune données géographique</h3>";
    }
}