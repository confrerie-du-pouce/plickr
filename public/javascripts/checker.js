"use strict";

window.onload = function() {
    let forms = document.getElementsByTagName('form');
    for (let form of forms) form.addEventListener('submit', function(e) {
        e.preventDefault();
        console.log(form);
        let data = new FormData(form);
        let button = form[form.length - 1];
        let xhr = new XMLHttpRequest();
        console.log('data');
        xhr.onload = () => {
            if (xhr.status === 200) {
                button.setAttribute('value', 'Envoyé !');
            }
        };
        xhr.open('POST', form.getAttribute('action'), true);
        xhr.send(data);
    });
};