<?php

use Plickr\Controllers\FrontController;
use Plickr\Utils\Request;
use Plickr\Utils\Response;

require_once __DIR__.'/vendor/autoload.php';

session_start();

$request = new Request($_GET, $_POST);
$response = new Response();
new FrontController($request, $response);