<?php
namespace Plickr;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class View
{
    protected $parts;
    protected $template;
    protected $twig;

    /**
     * View constructor.
     * @param $template
     * @param array $parts
     */
    public function __construct($template, array $parts = array())
    {
        $this->template = $template;
        $this->parts = $parts;
        $loader = new FilesystemLoader('templates/');
        $this->twig = new Environment($loader, [
            "debug" => true,
        ]);
    }

    public function template($name) {
        $this->template = $name;
    }

    public function set($name, $content) {
        $this->parts[$name] = $content;
    }

    public function render() {
        if (! isset($this->template)) $this->template('index');
        return $this->twig->render($this->template.'.html.twig', $this->parts);
    }

}