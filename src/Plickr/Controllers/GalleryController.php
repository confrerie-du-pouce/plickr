<?php


namespace Plickr\Controllers;


use Plickr\Model\Image;
use Plickr\Utils\Request;
use Plickr\Utils\Response;
use Plickr\View;

class GalleryController extends AbstractController
{
    private $images = array();

    /**
     * Gallery constructor.
     * @param Request $request
     * @param Response $response
     * @param View $view
     */
    public function __construct(Request $request, Response $response, View $view)
    {
        parent::__construct($request, $response, $view);
    }

    /**
     * @inheritDoc
     */
    public function defaultAction()
    {
        $this->includeImages();
    }

    public function loadImage() {
        $base_path = "public/images/full";
        if (is_dir($base_path)) {
            $files = scandir($base_path);
            foreach ($files as $file)
                if ($file != '.' && $file != '..')
                    $this->images[$file] = new Image($base_path . $file);
        }
    }

    public function includeImages(){
        $this->loadImage();
        $this->view->set('imgs', $this->images);
    }
}
