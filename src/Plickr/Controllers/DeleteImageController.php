<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/12/2019
 * Time: 15:20
 */

namespace Plickr\Controllers;


use Plickr\Model\DataBase;
use Plickr\Utils\Request;
use Plickr\Utils\Response;
use Plickr\View;

class deleteImageController extends AbstractController
{

    public function __construct(Request $request, Response $response, View $view)
    {
        parent::__construct($request, $response, $view);
    }

    public function deleteImage(){
        if(unlink('public/images/full/'.$this->request->get()['id'])){
            if(unlink('public/images/miniature/'.$this->request->get()['id'])){
                if(unlink('public/images/resized/'.$this->request->get()['id'])){
                    $this->view->set('feedback', "image suprimée");
                }
            }
        }else{
            $this->view->set('feedback', "la suppression a échouée");
        }
    }

    /**
     * Just call another function defined in case the controller doesn't know what to do.
     */
    public function defaultAction()
    {
        // TODO: Implement defaultAction() method.
    }
}