<?php


namespace Plickr\Controllers;


use Exception;
use Plickr\Model\Image;
use Plickr\Utils\Request;
use Plickr\Utils\Response;
use Plickr\View;

/**
 * Class ImageController
 * @package Plickr\Controllers
 * @todo Finir de récupérer les métadonnées dans le format voulu
 * @todo Voir pour la modification des métadonnées à l'aide d'un JSON
 */
class ImageController extends AbstractController
{

    /**
     * Image constructor.
     * @param Request $request
     * @param Response $response
     * @param View $view
     */
    public function __construct(Request $request, Response $response, View $view)
    {
        parent::__construct($request, $response, $view);
    }

    public function defaultAction()
    {
        $pic = new Image('public/images/full/'.$this->request->get()['id']);
        $this->view->set('img', $pic->getPath());
        $this->view->set('data', $pic->getData());
        $this->view->set('title', $pic->getTitle());
        $this->view->set('type', $pic->getData()['MIME Type']);
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")
            . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $this->view->set('url', $current_url);
        $this->view->set('url_picture', explode('?',$current_url)[0].'/'.$pic->getData()['Directory'].'/'.$pic->getData()['File Name']);
        $this->view->set('description', $pic->getDescription());
        $this->view->set('width', $pic->getData()['Image Width']);
        $this->view->set('height', $pic->getData()['Image Height']);
        if (isset($pic->getData()['GPS Latitude'])) {
            $lat = explode(' ', $pic->getData()['GPS Latitude']);
            $this->view->set('lat', $this->DMSToDecimal($lat[0], explode("'", $lat[2])[0], explode('"', $lat[3])[0]));
        } else
            $this->view->set('lat', 0);
        if (isset($pic->getData()['GPS Longitude'])) {
            $lon = explode(' ', $pic->getData()['GPS Longitude']);
            $this->view->set('lon', $this->DMSToDecimal($lon[0], explode("'", $lon[2])[0], explode('"', $lon[3])[0]));
        } else
            $this->view->set('lon', 0);
    }

    public function changeMetadata() {
        $image = new Image('public/images/full/'.$this->request->get()['pic']);
        $data = array();
        $data['path'] = $image->getPath();
        $data['file'] = $image->getFileName();
        $data['titre'] = $image->getTitle();
        $data['auteur'] = $image->getAuthor();
        $data['description'] = $image->getDescription();
        $data['state'] = $image->getState();
        $data['iso'] = $image->getISOState();
        $data['date'] = $image->getCreationDate();
        $data['rights'] = $image->getRights();
        $data['usage'] = $image->getUsage();
        $data['keywords'] = $image->getKeywords();
        $this->view->set('image', $data);
    }

    private function DMSToDecimal($degree, $minute, $second) {
        return $degree + $minute/60 + $second/3600;
    }
}
