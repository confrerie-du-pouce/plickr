<?php


namespace Plickr\Controllers;


use Plickr\Model\DataBase;
use Plickr\Model\Image;

class AddImageController extends AbstractController
{

    public function __construct($request, $response, $view)
    {
        parent::__construct($request,$response,$view);
    }

    public function upload(){
        $images = array();
        if(!empty($_FILES)) {
            $success = true;
            foreach ($_FILES as $image => $imagearray) {
                if (!exif_imagetype($imagearray['tmp_name'])) {
                    $this->view->set('feedback', "ce n'est pas une image");
                } else {
                    $pathinfo = pathinfo($imagearray['name']);
                    $imageName = uniqid();
                    $imageFull = $imageName  . "." . $pathinfo['extension'];
                    $images[] = 'public/images/full/'.$imageFull;
                    if (move_uploaded_file($imagearray['tmp_name'], "public/images/full/" . $imageFull)) {
                        $this->createThumbnailandResize($imageFull,$pathinfo['extension']);
                    }else{
                        $success = false;
                    }
                }
            }
            if ($success) {
                $this->check($images);
            } else {
                $this->view->set('feedback', 'upload failed');
            }
        }
    }

    public function createThumbnailandResize($imagePath, $extention){
        switch ($extention){
            case 'png':
                $sourceImage = imagecreatefrompng("public/images/full/" . $imagePath);
                break;
            case 'jpg':
            case 'jpeg':
                $sourceImage = imagecreatefromjpeg("public/images/full/" . $imagePath);
                break;
            case 'gif':
                $sourceImage = imagecreatefromgif("public/images/full/" . $imagePath);
                break;
            case 'bmp':
                $sourceImage = imagecreatefrombmp("public/images/full/" . $imagePath);
                break;
        }
        $virtualImageThumbnail = imagecreatetruecolor(100, 100);
        imagecopyresampled($virtualImageThumbnail,$sourceImage,0,0,0,0,100,100, 100, 100);
        imagejpeg($virtualImageThumbnail,"public/images/miniature/".$imagePath );
        $vitualImageResize = imagecreatetruecolor(imagesx($sourceImage)/2, imagesy($sourceImage)/2);
        imagecopyresampled($vitualImageResize,$sourceImage,0,0,0,0,imagesx($sourceImage)/2,imagesy($sourceImage)/2, imagesx($sourceImage), imagesy($sourceImage));
        imagejpeg($vitualImageResize, "public/images/resized/".$imagePath);
    }

    public function check($files){
        $this->view->template('checks');
        $images = array();
        foreach ($files as $file) {
            $image = array();
            $im = new Image($file);
            $image['path'] = $file;
            $image['file'] = $im->getFileName();
            $image['titre'] = $im->getTitle();
            $image['auteur'] = $im->getAuthor();
            $image['description'] = $im->getDescription();
            $image['state'] = $im->getState();
            $image['iso'] = $im->getISOState();
            $image['date'] = $im->getCreationDate();
            $image['rights'] = $im->getRights();
            $image['usage'] = $im->getUsage();
            $image['keywords'] = $im->getKeywords();
            $images[] = $image;
        }
        $this->view->set('images', $images);
    }

    /**
     * Met à jour les métadonnées reçu via HTTP POST.
     */
    public function update() {
        if (!empty($this->request->post())) {
            $sourceFile = $this->request->post()['path'];
            $titre = $this->request->post()['titre'];
            Image::setTitle($sourceFile, $titre);
            $auteur = $this->request->post()['auteur'];
            Image::setAuthor($sourceFile, $auteur);
            $description = $this->request->post()['description'];
            Image::setDescription($sourceFile, $description);
            $state = $this->request->post()['state'];
            Image::setState($sourceFile, $state);
            $iso = $this->request->post()['iso'];
            Image::setIso($sourceFile, $iso);
            $date = $this->request->post()['date'];
            Image::setDate($sourceFile, $date);
            $rights = $this->request->post()['rights'];
            Image::setRights($sourceFile, $rights);
            $usage = $this->request->post()['usage'];
            Image::setUsage($sourceFile, $usage);
            $keywords = $this->request->post()['keywords'];
            Image::setKeywords($sourceFile, $keywords);
            unlink($sourceFile.'_original');
        }
    }

    public function defaultAction()
    {
        $this->upload();
    }
}