<?php


namespace Plickr\Controllers;


use Exception;
use Plickr\Utils\Request;
use Plickr\Utils\Response;
use Plickr\Utils\Router;
use Plickr\View;
use Plickr\Controllers\GalleryController;
use Plickr\Controllers\ImageController;
use Plickr\Controllers\ConnexionController;

class FrontController
{
    private $request, $response;

    /**
     * Front constructor.
     * @param $request
     * @param $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->exec();
    }

    /**
     * Cette fonction permet de sélectionner le controller selon la page demandé et de récupérer
     * le contenu à envoyer à l'utilisateur.
     */
    public function exec() {
        $router = new Router($this->request);
        $view = new View($router->template());
        try {
            if($router->classname() !== "about"){
                $className = "Plickr\\Controllers\\" . $router->classname();
                $action = $router->action();
                $controller = new $className($this->request, $this->response, $view);
                $controller->exec($action);
            }
        } catch (Exception $exception) {
            $view->set('title', 'Erreur');
            $view->set('content', "Une erreur d'exécution s'est produite.");
        }
        $view->set('session', $_SESSION);
        $content = $view->render();
        $this->response->send($content);
    }
}
