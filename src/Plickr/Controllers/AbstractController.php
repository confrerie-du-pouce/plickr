<?php


namespace Plickr\Controllers;


use Plickr\Utils\Request;
use Plickr\Utils\Response;
use Plickr\View;

abstract class AbstractController
{
    protected $request, $response, $view;

    /**
     * AbstractController constructor.
     * @param $request
     * @param $response
     * @param $view
     */
    public function __construct(Request $request, Response $response, View $view)
    {
        $this->request = $request;
        $this->response = $response;
        $this->view = $view;
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")
            . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $view->set('base_url', explode('?', $current_url)[0]);
    }


    /**
     * Execute an action define above and corresponding to the string parameter.
     * @param $action
     */
    public function exec($action)
    {
        if (method_exists($this, $action))
            $this->$action();
    }

    /**
     * Just call another function defined in case the controller doesn't know what to do.
     */
    public abstract function defaultAction();
}
