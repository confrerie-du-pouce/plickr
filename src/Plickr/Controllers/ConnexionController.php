<?php

namespace Plickr\Controllers;


use Plickr\Model\DataBase;

class ConnexionController extends AbstractController
{
    private $compte = array("Alexandre" => "niveau", "Jean-Marc" => "lecarpentier", "Lucas" => "tourniere", "Clement" => "vetillard");

    public function __construct($request, $response, $view)
    {
        parent::__construct($request,$response,$view);
    }

    public function login()
    {
        $post = $this->request->post();
        if(count($post)!== 0) {
            if (isset($this->compte[$post['username']])) {
                if ($post['password'] === $this->compte[$post['username']]) {
                    $_SESSION['user']['username'] = $post['username'];
                    $this->view->set("feedback", "connecté avec succés");
                } else {
                    $this->view->set("feedback", "mauvais mot de passe");
                }
            } else {
                $this->view->set("feedback", "le compte n'existe pas");
            }
        }
    }

    public function logout()
    {
        session_destroy();
        session_start();
        $this->view->set("feedback", "déconnecté avec succés");
    }


    /**
     * Just call another function defined in case the controller doesn't know what to do.
     */
    public function defaultAction()
    {
        // TODO: Implement defaultAction() method.
    }
}