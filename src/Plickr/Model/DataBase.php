<?php

namespace Plickr\Model;


class DataBase
{

    private $users;
    private $usersimages;

    public function __construct()
    {
        $jsonfilecontent = file_get_contents("public/databases/users.json");
        $jsonfilecontent2 = file_get_contents("public/databases/usersimages.json");
        $this->users = json_decode($jsonfilecontent,true);
        $this->usersimages = json_decode($jsonfilecontent2, true);
    }

    public function addUser($username, $password)
    {
        $hash = password_hash($password, PASSWORD_BCRYPT);
        $this->users[$username] = $hash;
        $this->usersimages[$username] = array();
        $jsonfilecontent = json_encode($this->users);
        $jsonfilecontent2 = json_encode($this->usersimages);
        file_put_contents('public/databases/users.json', $jsonfilecontent);
        file_put_contents('public/databases/usersimages.json', $jsonfilecontent2);
    }

    public function userInDB($username)
    {
        return isset($this->users[$username]);
    }

    public function rightPassword($username, $password)
    {
        return password_verify($password, $this->users[$username]);
    }

    public function getUserImages($username)
    {
        return $this->usersimages[$username];
    }

    public function addUserImage($username, $imageName)
    {
        if(!isset($this->usersimages[$username])){
            $imagearray = array($imageName);
            $this->usersimages[$username] = $imagearray;
        }else{
            array_push($this->usersimages[$username], $imageName);
        }
        $jsonfilecontent = json_encode($this->usersimages);
        file_put_contents('public/databases/usersimages.json', $jsonfilecontent);
    }

    public function deleteUserImage($username, $imageName){
        if(isset($this->usersimages[$username])){
            for($i = 0; $i<count($this->usersimages[$username]); $i++){
                if($this->usersimages[$username][$i] === $imageName){
                    array_splice($this->usersimages[$username],$i,1);
                }
            }
        }
        $jsonfilecontent = json_encode($this->usersimages);
        file_put_contents('public/databases/usersimages.json', $jsonfilecontent);
    }
}