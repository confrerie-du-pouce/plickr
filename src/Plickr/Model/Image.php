<?php


namespace Plickr\Model;

/**
 * Class Image
 * @package Plickr\Model
 * @todo Stocker les infos dans $_SESSION pour éviter les traitements redondant
 */
class Image
{
    private $path;
    private $metadata;

    /**
     * Picture constructor.
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
        $this->metadata = $this->metadata();
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    public function metadata() {
        $result = array();
        exec("exiftool " . $this->path, $result);
        $parsed = array();
        foreach ($result as $line) {
            $values = explode(' : ', $line);
            if (isset($values[1]))
                $parsed[trim($values[0])] = $values[1];
        }
        return $parsed;
    }

    public function getTitle() {
        if (isset($this->metadata['Title']))
            return $this->metadata['Title'];
        elseif (isset($this->metadata['ObjectName']))
            return $this->metadata['ObjectName'];
        else
            return '';
    }

    public function getFileName() {
        if (isset($this->metadata['SourceFile']))
            return $this->metadata['SourceFile'];
        elseif (isset($this->metadata['FileSource']))
            return $this->metadata['FileSource'];
        else
            return '';
    }

    public function getState() {
        if (isset($this->metadata['State']))
            return $this->metadata['State'];
        elseif (isset($this->metadata['Province-State']))
            return $this->metadata['Province-State'];
        else
            return '';
    }

    public function getISOState() {
        if (isset($this->metadata['Country']))
            return $this->metadata['Country'];
        elseif (isset($this->metadata['Country-PrimaryLocationName']))
            return $this->metadata['Country-PrimaryLocationName'];
        else
            return '';
    }

    public function getCreationDate() {
        if (isset($this->metadata['Date Created']))
            return $this->metadata['Date Created'];
        elseif (isset($this->metadata['Create Date']))
            return $this->metadata['Create Date'];
        else
            return '';
    }

    public function getRights() {
        if (isset($this->metadata['CopyrightNotice']))
            return $this->metadata['CopyrightNotice'];
        elseif (isset($this->metadata['Copyright']))
            return $this->metadata['Copyright'];
        elseif (isset($this->metadata['Rights']))
            return $this->metadata['Rights'];
        else
            return '';
    }

    public function getUsage() {
        if (isset($this->metadata['Usage Terms']))
            return $this->metadata['Usage Terms'];
        else
            return '';
    }

    public function getDescription() {
        if (isset($this->metadata['Description']))
            return $this->metadata['Description'];
        elseif (isset($this->metadata['Caption-Abstract']))
            return $this->metadata['Caption-Abstract'];
        elseif (isset($this->metadata['ImageDescription']))
            return $this->metadata['ImageDescription'];
        else
            return '';
    }

    public function getAuthor() {
        if (isset($this->metadata['By-line']))
            return $this->metadata['By-line'];
        elseif (isset($this->metadata['Creator']))
            return $this->metadata['Creator'];
        elseif (isset($this->metadata['Artist']))
            return $this->metadata['Artist'];
        else
            return '';
    }

    public function getKeywords() {
        if (isset($this->metadata['Subject']))
            return $this->metadata['Subject'];
        elseif (isset($this->metadata['Keywords']))
            return $this->metadata['Keywords'];
        else
            return '';
    }

    public function __toString()
    {
        return $this->path;
    }

    public function getData() {
        return $this->metadata;
    }

    public static function setTitle($path, $value) {
        exec('exiftool -xmp:Title="' . $value . '"'
            . ' -iptc:ObjectName="' . $value . '"'
            . ' ' . $path
        );
    }

    public static function setDescription($path, $value) {
        exec('exiftool -xmp:Description="' . $value . '"'
            . ' -iptc:Caption-Abstract="' . $value . '"'
            . ' -exif:ImageDescription="' . $value . '"'
            . ' ' . $path
        );
    }

    public static function setState($path, $value) {
        exec('exiftool -xmp:State="' . $value . '"'
            . ' -iptc:Province-State="' .$value . '"'
            . ' ' . $path
        );
    }

    public static function setIso($path, $value) {
        exec('exiftool -xmp:Country="' . $value . '"'
            . ' -iptc:Country-PrimaryLocationName="' . $value . '"'
            . ' ' . $path
        );
    }

    public static function setDate($path, $value) {
        exec('exiftool -xmp:DateCreated="' . $value . '"'
            . ' -exif:CreateDate="' . $value . '"'
            . ' ' . $path
        );
    }

    public static function setRights($path, $value) {
        exec('exiftool -xmp:rights="' . $value . '"'
            . ' -iptc:CopyrightNotice="' . $value . '"'
            . ' -exif:Copryright="' . $value . '"'
            . ' ' . $path);
    }

    public static function setUsage($path, $value) {
        exec('exiftool -xmp:UsageTerms="' . $value . '" ' . $path);
    }

    public static function setAuthor($path, $value) {
        exec('exiftool -xmp:Creator="' . $value . '"'
            . ' -iptc:By-line="' . $value . '"'
            . ' -exif:Artist="' . $value . '"'
            . ' ' . $path
        );
    }

    public static function setKeywords($path, $value) {
        exec('exiftool -xmp:Subject="' . $value . '"'
            . ' -iptc:Keywords="' . $value . '"'
            . ' ' . $path
        );
    }
}
