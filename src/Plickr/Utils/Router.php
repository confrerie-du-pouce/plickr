<?php
namespace Plickr\Utils;


class Router
{
    private $request;

    /**
     * Router constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function classname() {
        if (key_exists('p', $this->request->get())) {
            switch ($this->request->get()['p']) {
                case 'login':
                case 'logout':
                    return 'ConnexionController';
                case 'details':
                case 'metadata':
                    return 'ImageController';
                case 'upload':
                    return 'AddImageController';
                case 'delete':
                    return 'DeleteImageController';
                case 'about':
                    return 'about';
                default:
                    return 'GalleryController';
            }
        }
        return 'GalleryController';
    }

    public function action() {
        if (key_exists('p', $this->request->get())) {
            switch ($this->request->get()['p']) {
                case 'login':
                    return 'login';
                case 'logout':
                    return 'logout';
                case 'userimages':
                    return 'includeImages';
                case 'delete':
                    return 'deleteImage';
                case 'upload':
                    if (key_exists('a', $this->request->get()))
                        if ($this->request->get()['a'] == 'validate')
                            return 'update';
                    break;
                case 'metadata':
                    return 'changeMetadata';
            }
        }
        return 'defaultAction';
    }

    public function template() {
        if (key_exists('p', $this->request->get())) {
            switch ($this->request->get()['p']) {
                case 'login':
                    return 'connexion';
                case 'logout':
                    return 'logout';
                case 'details':
                    return 'details';
                case 'upload':
                    return 'upload';
                case 'userimages':
                case 'delete':
                    return 'userimages';
                case 'about':
                    return 'about';
                case 'metadata':
                    return 'changeMetadata';
            }
        }
        return 'gallery';
    }
}