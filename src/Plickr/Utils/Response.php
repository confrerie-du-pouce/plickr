<?php


namespace Plickr\Utils;

/**
 * Class Response
 *
 * Support HTTP header feature.
 *
 * @package Plickr\Utils
 */
class Response
{
    /**
     * @var array HTTP headers to send
     */
    private $headers = array();

    /**
     * @param $value
     */
    public function addHeader($value) {
        $this->headers[] = $value;
    }

    /**
     * Send all headers saved
     */
    public function sendHeaders()
    {
        foreach ($this->headers as $header) {
            header($header);
        }
    }

    /**
     * @param $content string Send the response after sending the headers.
     */
    public function send($content) {
        $this->sendHeaders();
        echo $content;
    }
}