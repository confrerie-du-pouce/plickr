<?php
namespace Plickr\Utils;


class Request
{
    private $get;
    private $post;

    /**
     * Construit l'objet en récupérant les données transmises par la requête HTTP.
     * @param $get
     * @param $post
     */
    public function __construct($get, $post)
    {
        if ($get != null)
            $this->get = $get;
        else $this->get = array();
        if ($post != null)
            $this->post = $post;
        else $this->post = array();
    }

    public function get() {
        return $this->get;
    }

    public function post() {
        return $this->post;
    }
}