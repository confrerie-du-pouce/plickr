# Web

Projet de web du S1 de M1 IDC présentant une bibliotheque d'images

## Fonctionnalité

- Parcourir la bibliotheque d'images.
- Ajouter des images.
- Affichage des métadonnées d'une image sélectionnée dans la liste ( dont une carte si il y a des données géographiques )
